#!/usr/bin/env python
#coding:utf-8


import os
import time
import datetime
import hashlib
import logging
import json

import tornado.web
import tornado.escape

from base        import BaseHandler
from lib.models  import Custom,Promo,User,City,Zone,Tag,AuthLog,Summary
from lib.helpers import save_img,save_file,generate_ymd,check_extensions,new_alchemy_encoder




class InvalidInputException(Exception):
    pass




class BaseManagerHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(BaseManagerHandler, self).__init__(*args, **kwargs)
        # self.jinja2.globals.update(xsrf_form_html=self.xsrf_form_html())


    def get_current_user(self):
        username = self.get_secure_cookie('user')
        if username:
            username = tornado.escape.json_decode(username)
            user = self.db.query(User).filter_by(name=username).first()
            # return False if not user else user
            if not user:
                self.clear_cookie('user')
                return False
            else:
                return user
        else:
            return False


    @tornado.web.authenticated
    def prepare(self):
        pass

    # def resp_message(self,success=False,**kwargs):
    #     #返回处理结果 True False
    #     resp = dict(success = success)
    #     resp.update(kwargs)

    #     self.write(tornado.escape.json_encode(resp))




class CustomListHandler(BaseManagerHandler):
    def get(self):
        city_code   = self.get_argument('city_code',None)
        zone_code   = self.get_argument('zone_code',None)
        tag_code    = self.get_argument('tag_code',None)
        customs     = None
        custom_list = []
        if (city_code and zone_code and tag_code):
            customs = self.db.query(Custom).filter_by(
                                            city_code = city_code,
                                            zone_code  = zone_code,
                                            tag_code   = tag_code,
                                            ).all()
            for custom in customs:
                custom_list.append(custom.to_json())

        self.set_header("Content-Type", "application/json")
        self.write(tornado.escape.json_encode({'custom_list':custom_list}))




class CustomInfoHandler(BaseManagerHandler):
    @tornado.web.removeslash
    def get(self):
        custom_code = self.get_argument('custom_code',None)
        custom = self.db.query(Custom).filter_by(custom_code=custom_code,status=True).first()
        zone   = None
        if custom:
            zones        = self.db.query(Zone).filter_by(city_code=custom.city_code).all()
            custom.zones = zones

        self.set_header("Content-Type", "application/json")
        self.write(
            json.dumps(
                custom,
                cls = new_alchemy_encoder(
                    False,
                    fields_to_expand = ['zones'],
                    ignore           = ['to_json','to_dict']
                    ),
                check_circular=True
            )
        )
        # self.write(
        #     tornado.escape.json_encode({'custom':json.dumps(custom,cls = new_alchemy_encoder(False,fields_to_expand = [],ignore = ['to_json','to_dict']),check_circular=False)})
        #     )




class ManagerIndexHandler(BaseManagerHandler):
    def get(self):
        citys   = self.db.query(City).order_by(City.id.asc()).all()
        zones   = self.db.query(Zone).filter_by(city_code=citys[0].code).order_by(Zone.id.asc()).all()
        tags    = self.db.query(Tag).order_by(Tag.id.asc()).all()
        self.render('backend_index.html',citys=citys,zones=zones,tags=tags)




class CustomManagerHandler(BaseManagerHandler):
    
    # @tornado.web.authenticated
    @tornado.web.removeslash
    def get(self,custom_code=None):
        if custom_code:
            self.send_error(status_code=405)

        citys   = self.db.query(City).order_by(City.id.asc()).all()
        zones   = self.db.query(Zone).filter_by(city_code=citys[0].code).order_by(Zone.id.asc()).all()
        tags    = self.db.query(Tag).order_by(Tag.id.asc()).all()
        if citys and zones and tags:
            customs = self.db.query(Custom).filter_by(
                                            city_code = citys[0].code,
                                            zone_code = zones[0].code,
                                            tag_code  = tags[0].code
                                            ).order_by(Custom.id.asc()).all()
            self.render('backend_custom_manager.html',citys=citys,zones=zones,tags=tags,customs=customs)
        else:
            self.render('backend_custom_manager.html',citys=citys,zones=zones,tags=tags)


    def post(self,custom_code=None):     #修改商户信息
        if custom_code:
            self.send_error(status_code=405)
        else:
            city_code   = self.get_argument('city_code',None)
            zone_code   = self.get_argument('zone_code',None)
            tag_code    = self.get_argument('tag_code',None)
            custom_code = self.get_argument('custom_code',None)
            custom_name = self.get_argument('custom_name',None)
            last_url    = self.get_argument('custom_img_url',None)
            custom      = self.db.query(Custom).filter_by(custom_code=custom_code,status=True).first()
            try:
                if not custom:
                    self.resp_message(success=False,message=u'No Custom')
                else:
                    custom.city_code   = city_code   if city_code   else custom.city_code
                    custom.zone_code   = zone_code   if zone_code   else custom.zone_code
                    custom.tag_code    = tag_code    if tag_code    else custom.tag_code
                    custom.custom_name = custom_name if custom_name else custom.custom_name
                    custom.last_url    = last_url    if last_url    else custom.last_url
                    self.db.commit()
                    self.redis.update_custom(code=custom_code,name=custom_name)
                    self.resp_message(success=True,message=u'Edit Custom Successful')
            except Exception,e:
                logging.error(e)
                self.resp_message(success=False,message=u'Invalid Input')


    def put(self,custom_code=None):      # 添加商户
        if custom_code:
            self.send_error(status_code=405)

        city_code   = self.get_argument('city_code',None)
        zone_code   = self.get_argument('zone_code',None)
        tag_code    = self.get_argument('tag_code',None)
        custom_code = self.get_argument('custom_code',None)
        custom_name = self.get_argument('custom_name',None)
        last_url    = self.get_argument('custom_img_url',None)
        custom      = self.db.query(Custom).filter_by(custom_code=custom_code).first()
        try:
            if custom:
                self.resp_message(success=False,message=u'Custom Exist')
            else:
                custom = Custom(
                    city_code   = city_code,
                    zone_code   = zone_code,
                    tag_code    = tag_code,
                    custom_code = custom_code,
                    custom_name = custom_name,
                    last_url    = last_url if last_url else 'img/logo.png'
                    )
                self.db.add(custom)
                self.db.commit()
                self.redis.update_custom(code=custom_code,name=custom_name,action=u'add')
                self.resp_message(success=True,message=u'Add Custom Success')
        except Exception,e:
            logging.error(e)
            self.resp_message(success=False,message=u'Invalid Input')


    def delete(self,custom_code=None):
        if not custom_code:
            self.resp_message(success=True,message=u'Custom ID Need')
        else:
            success = False
            message = u'Custom Not Exist!'
            custom = self.db.query(Custom).filter_by(custom_code=custom_code,status=True).first()
            if custom:
                try:
                    custom.status = False
                    self.db.commit()
                    self.redis.update_custom(code=custom_code,name=None,action=u'delete')
                    success = True
                    message = u'Delete Custom Success!'
                except Exception,e:
                    logging.error(e)
            self.resp_message(success=success,message=message)

    # def post(self):
    #     _method = self.get_argument('_method',None)
    #     if _method == u'post':
    #         # 添加商户
    #         city_code   = self.get_argument('city_code',None)
    #         zone_code   = self.get_argument('zone_code',None)
    #         tag_code    = self.get_argument('tag_code',None)
    #         custom_code = self.get_argument('custom_code',None)
    #         custom_name = self.get_argument('custom_name',None)
    #         custom = self.db.query(Custom).filter_by(custom_code=custom_code).first()
    #         if custom:
    #             self.write('此商户ID已存在')
    #         else:
    #             custom = Custom(
    #                 city_code   = city_code,
    #                 zone_code   = zone_code,
    #                 tag_code    = tag_code,
    #                 custom_code = custom_code,
    #                 custom_name = custom_name
    #                 )
    #             self.db.add(custom)
    #             self.db.commit()
    #             self.write('添加商户成功！')
    #     elif _method == u'put':
    #         # 上传图片
    #         custom_id   = self.get_argument('custom_id',None)
    #         upload_file = self.request.files['upload_file'][0]
    #         if (custom_id and upload_file):
    #             custom       = self.db.query(Custom).filter_by(id=custom_id).first()
    #             if custom:
    #                 file_name       = upload_file['filename']
    #                 year,mon,day    = generate_ymd()
    #                 path            = os.path.join('include','custom',custom.custom_code,year,mon,day)
    #                 img_url         = os.path.join(path,file_name)
    #                 file_path       = os.path.join(os.path.abspath('.'),self.application.settings['static_path'],path)
    #                 save_img(
    #                     file_path   = file_path,
    #                     upload_file = upload_file
    #                     )
    #                 custom.last_url = img_url
    #                 self.db.commit()
    #                 self.write('Save img')
    #             else:
    #                 self.write('Custom not found')
    #     else:
    #         self.send_error(405,log_message="Method not support for this!")




class ZoneImgUpdateHandler(BaseManagerHandler):
    def post(self):
        success = False
        message = None

        city_code = self.get_argument('city_code',None)
        zone_code = self.get_argument('zone_code',None)
        tag_code  = self.get_argument('tag_code',None)
        last_url  = self.get_argument('custom_img_url',None)

        if (city_code and zone_code and tag_code and last_url):
            promo_id = city_code + zone_code + tag_code
            promo    = self.db.query(Promo).filter_by(promo_id=promo_id).first()
            if promo:
                promo.last_url = last_url
                self.db.commit()
                success = True
                message = u'更改区域图片成功'
            else:
                promo = Promo(
                    promo_id  = promo_id,
                    city_code = city_code,
                    zone_code = zone_code,
                    tag_code  = tag_code,
                    last_url  = last_url
                )
                self.db.add(promo)
                self.db.commit()
                success = True
                message = u'更改区域图片成功'
        self.resp_message(success=success,message=message)





class UploadFileHandler(BaseManagerHandler):
    """文件上传"""

    def post(self):
        success = False     #Init Response Message
        message = None
        img_url = None

        _fileUpload = self.request.files['fileUpload'][0]       #handle upload files
        _filename   = _fileUpload['filename']
        if not isinstance(_filename,str):
            _filename = _filename.encode('utf-8')
        if check_extensions(_filename):
            new_filename = hashlib.sha1(_filename + str(time.time())).hexdigest() \
                            + '.' \
                            + _filename.split('.')[-1].lower()

            path        = os.path.join('include','img')
            file_path = os.path.join(os.path.abspath('.'),self.application.settings['static_path'],path)
            if save_file(path=file_path,name=new_filename,upfile=_fileUpload):
                img_url     = os.path.join(path,new_filename)
                success = True
            else:
                message = u'Something was wrong with save_file!'
        else:
            message = u'File Extension Not Supported!'
        self.resp_message(success=success,message=message,img_url=img_url)





class AnalysisHandler(BaseManagerHandler):
    def get(self):
        self.render('backend_analysis.html')


    def post(self):
        now         = datetime.date.today()
        page        = self.get_argument('page',0)
        page_size   = self.get_argument('size',10)
        query_date  = self.get_argument('q_date',None)
        left_index  = None

        try:
            
            page_size  = int(page_size) if (isinstance(page_size,unicode) and int(page_size) > 0) else 10
            left_index = (int(page) - 1) * page_size if (isinstance(page,unicode) and int(page) > 0) else 0
        except Exception,e:
            logging.error(e)

        resp_data   = dict()
        # c_logs      = []
        custom_list = self.redis.lrange('custom_code',left_index,left_index + page_size - 1)
        c_logs      = self.redis.get_analysis(custom_list,query_date)
        
        # for custom in custom_list:
        #     custom_name = self.redis.get('custom:%s:name' % custom)
        #     auth,promo  = self.redis.hmget(
        #         'authlog:%s' % now,
        #         '%s:auth:%s' % (custom,now),
        #         '%s:promo:%s' %(custom,now)
        #         )
        #     log = {
        #         'custom_code':custom,
        #         'custom_name':custom_name,
        #         'auth_times':int(auth) if auth else 0,
        #         'promo_times':int(promo) if promo else 0
        #         }
        #     c_logs.append(log)

        resp_data['max_size'] = self.redis.llen('custom_code')
        resp_data['analysis'] = c_logs
        self.set_header("Content-Type", "application/json")
        self.write(tornado.escape.json_encode(resp_data))

        # NOW = datetime.date.today()
        # custom_code = self.get_argument('custom_code',None)
        # left_time   = self.get_argument('left_time',NOW)
        # right_time  = self.get_argument('right_time',NOW + datetime.timedelta(days=1))
        # page        = self.get_argument('page',0)
        # size        = self.get_argument('size',10)

        # analysis  = None
        # max_size  = None
        # resp_data = dict()

        # try:
            
        #     size        = int(size) if (isinstance(size,unicode) and int(size) > 0) else 10
        #     page        = (int(page) - 1) * size if (isinstance(page,unicode) and int(page) > 0) else 0
            

        #     max_size = self.db.query(Custom).filter_by(status=1).count()
        #     total_auth = self.db.query(AuthLog).filter(AuthLog.log_time > '2013-11-01').count()
        #     resp_data['max_size'] = max_size
        #     resp_data['totals']   = total_auth

        #     analysis = self.db.bind.execute('''
        #         select custom.custom_code,custom.custom_name,
        #         sum(case when auth_log.methods='auth' then 1 else 0 end) as auth_times,
        #         sum(case when auth_log.methods='promo' then 1 else 0 end) as promo_times
        #         from custom
        #         left join auth_log 
        #         on auth_log.custom_code = custom.custom_code
        #         and auth_log.log_time between %s and %s
        #         where custom.status = 1
        #         group by custom.custom_code
        #         limit %s,%s
        #         ''' , (left_time,right_time,page,size)).fetchall()

        #     dic = []
        #     import  decimal
        #     for a in analysis:
        #         _dic = dict((x,int(y) if isinstance(y,decimal.Decimal) else y) for x,y in a.items())
        #         dic.append(_dic)

        #     resp_data['analysis'] = dic

        #     self.set_header("Content-Type", "application/json")
        #     self.write(tornado.escape.json_encode(resp_data))
        #     # self.render('backend_analysis.html',analysis=analysis)
        # except Exception,e:
        #     logging.error(e)
        #     self.write('OOPS')




# class CityListHandler(BaseHandler):
#     """docstring for CityHandler"""
#     def get(self):
#         citys = self.db.query(City).order_by(City.id.asc()).all()
#         city_list = []
#         for city in citys:
#             city_list.append(city.to_json())

#         self.set_header("Content-Type", "application/json")
#         self.write(tornado.escape.json_encode({'city_list':city_list}))


class ZoneListHandler(BaseManagerHandler):
    def get(self):
        city_code = self.get_argument('city_code','0028')
        zones = self.db.query(Zone).filter_by(city_code=city_code).order_by(Zone.id.asc()).all()
        zone_list = []
        for zone in zones:
            zone_list.append(zone.to_json())

        self.set_header("Content-Type", "application/json")
        self.write(tornado.escape.json_encode({'zone_list':zone_list}))



class UAFilterListHandler(BaseHandler):
    def get(self):
        left_time  = now = datetime.date.today()
        right_time = left_time + datetime.timedelta(days=1)
        max_times  = 20

        # left_time = self.get_argument('left_time',None)
        # right_time = '2013-12-01

        logging.info('request for UA_Filters from %s' % self.request.remote_ip)

        try:
            UA_List = self.db.execute('''
                select count(UA) as UA_Times,UA
                from auth_log
                where log_time between :left_time and :right_time
                group by UA
                having UA_Times > :max_times
                order by UA_Times desc
                ''' 
                , {'left_time':left_time,'right_time':right_time,'max_times':max_times}
                ).fetchall()

            UA_Filters = []
            for ua in UA_List:
                UA_Filters.append(ua.UA)
                self.write(ua.UA+'\r\n')
        except Exception,e:
            logging.error(e)
        self.finish()




class ManagerSummaryHandler(BaseManagerHandler):
    def get(self):
        summarys = self.db.query(Summary).filter_by(deleted=False).order_by('id desc').limit(6).all()
        self.render('backend-recommend.html',summarys=summarys)


    def post(self):
        success = False
        message = None
        action  = self.get_argument('action',None)
        if action and action == u'changeDisplay':
            summaryID = self.get_argument('summaryID',None)
            active    = self.get_argument('display',False)
            active    = True if active and active == u'true' else False

            summary   = self.db.query(Summary).filter_by(id=summaryID).first()
            if summary:
                try:
                    # s = Summary(id=summaryID,active=active)
                    # self.db.merge(s)
                    summary.active = active
                    self.db.commit()
                    success = True
                    message = u'changeDisplay Success!'
                except Exception,e:
                    logging.error(e)
        self.resp_message(success=success,message=message)

    def put(self):
        success = False
        message = None

        title  = self.get_argument('summary_title',None)
        source = self.get_argument('summary_source',None)
        link   = self.get_argument('summary_link',None)
        active = self.get_argument('summary_active',None)
        if title and source and link and active:
            try:
                summary = Summary(
                    title  = title,
                    source = source,
                    link   = link,
                    active = True if active and active == u'true' else False
                    )
                self.db.add(summary)
                self.db.commit()
                success = True
                message = u'添加成功'
            except Exception,e:
                logging.error(e)
        self.resp_message(success=success,message=message)




class LoginHandler(BaseHandler):
    def get(self):
        user = self.get_secure_cookie('user',None)
        if user:
            self.redirect(self.reverse_url('backend'))
        self.render('backend_login.html',title='Login')

    def post(self):
        success = False     #初始化返回信息
        message = None

        username    = self.get_argument('username',None)   #获取登录信息
        password    = self.get_argument('password',None)
        remember_me = self.get_argument('remember_me',None)
        logging.info("User: %r from %r try to log in system" % (username,self.request.remote_ip))

        if self.if_user(username,password):     #认证
            logging.info("User: %r from %r log in Success" % (username,self.request.remote_ip))
            self.set_current_user(user=username,remember_me=remember_me)
            # self.redirect(self.get_argument('next',None) or self.reverse_url('backend'))
            success = True
            message = u'Login Success!'
        else:
            logging.warn("User %r from %r log failed!" % (username,self.request.remote_ip))
            # self.redirect(self.reverse_url('login'))
            success =  False
            message = u'Invalid Username or Password!'
        self.resp_message(success=success,message=message)

    def set_current_user(self,user,remember_me=False):
        expires_days = None if not remember_me else 30
        logging.info("Set cookie for User %r with IP_Addrs %r" % (user,self.request.remote_ip))
        self.set_secure_cookie("user",tornado.escape.json_encode(user),expires_days=expires_days)

    def if_user(self,name,passwd):
        try:
            user = self.db.query(User).filter_by(name=name).first()
            # logging.error('name:%r--->passwd:%r' % (user.name,user.passwd))
            if (user and user.name == name and user.passwd == passwd):
                return True
        except Exception,e:
            logging.error(e)
            return False


class LogoutHandler(BaseHandler):
    def get(self):
        user = self.get_secure_cookie('user')
        if user:
            self.clear_cookie('user')
        self.redirect(self.reverse_url('backend'))