#!/usr/bin/env python
#coding:utf-8


import logging
import json
import pdb
import time
import datetime

import tornado.escape
import tornado.gen
import tornado.web

from  sqlalchemy import func

from base        import BaseHandler
from lib.models  import Custom,City,Zone,Promo,AuthLog,Summary,SummaryClickCounter
from lib.helpers import new_alchemy_encoder,async




@async
def add_Authlog(db,custom_code,methods,request):
    # time.sleep(10)
    try:        #请求认证统计 
        authlog = AuthLog(
            custom_code = custom_code,
            methods     = methods,
            ip_addrs    = request.remote_ip,
            UA          = request.headers.get('User-Agent','-'),
            log_time    = datetime.datetime.now()
            )
        db.merge(authlog)
        db.commit()
    except Exception,e:
        logging.error(e)
        pass


class IndexHandler(BaseHandler):
    def get(self):
        self.render('index.html')


class CustomHandler(BaseHandler):

    def get(self):
        custom_code = router_id = self.get_argument('router_id',None)
        custom      = self.db.query(Custom).filter_by(custom_code=router_id).first()
        today_log   = 'authlog:%s' % datetime.date.today()

        # 如果custom 或 custom.last_url不存在 跳转到首页
        if (not custom or not custom.last_url):
            self.flash(u'OOPS! 好像有点问题唷！')
            self.redirect(self.reverse_url('index'))
        else:
            try:
                add_Authlog(db=self.db,custom_code=router_id,methods='auth',request=self.request)

                self.redis.authlog(methods='auth',custom_code=custom_code)
                
                # self.redis.hincrby(today_log,'%s:auth:%s' % (custom_code,datetime.date.today()) , 1)
                # print self.redis.ttl(today_log)
                # if self.redis.ttl(today_log) < 0:
                #     self.redis.expire(today_log,86400)
            except Exception,e:
                logging.error(e)
            analysis = dict(
                custom_code = router_id,
                creat_time  = time.mktime(time.gmtime())
                )
            self.set_secure_cookie('analysis',tornado.escape.json_encode(analysis))
            self.render('custom.html',custom=custom)
        # print self.request.request_time() * 1000


class PromoHandler(BaseHandler):
    def get(self):
        # args = dict(
        #     promo_code = 1234,
        #     city_code  = self.get_argument('city_code',None),
        #     zone_code  = self.get_argument('zone_code',None),
        #     tag_code   = self.get_argument('tag_code',None),
        #     callback  = self.get_argument('callback',None)
        #     )
        city_code = self.get_argument('city_code',None)
        zone_code = self.get_argument('zone_code',None)
        tag_code  = self.get_argument('tag_code',None)
        callback  = self.get_argument('callback',None)
        today_log = 'authlog:%s' % datetime.date.today()
        
        promo     = None
        news      = None

        if (city_code and zone_code and tag_code):
            promo = self.db.query(Promo).filter_by(
                                            city_code = city_code,
                                            zone_code = zone_code,
                                            tag_code  = tag_code).first()
            try:    #认证统计
                analysis = self.get_secure_cookie('analysis',None)
                analysis = tornado.escape.json_decode(analysis) if analysis else analysis
                custom_code = analysis['custom_code'] if analysis else 'unknown'
                add_Authlog(db=self.db,custom_code=custom_code,methods='promo',request=self.request)
                # authlog = AuthLog(
                #     custom_code = custom_code,
                #     methods     = 'promo',
                #     ip_addrs    = self.request.remote_ip,
                #     UA          = self.request.headers.get('User-Agent','-'),
                #     log_time    = datetime.datetime.now()
                #     )
                # self.db.merge(authlog)
                # self.db.commit()

                self.redis.authlog(methods='promo',custom_code=custom_code)

                # self.redis.hincrby(today_log,'%s:promo:%s' % (custom_code,datetime.date.today()) , 1)
                # if self.redis.ttl(today_log) < 0:
                #     self.redis.expire(today_log,86400)
            except Exception,e:
                logging.error(e)

        try:
            news = self.db.query(Summary).filter_by(active=True,deleted=False).order_by('id desc').limit(3).all()
        except Exception,e:
            logging.error(e)

        if not callback:
            callback = 'http://192.168.0.1:6789/web.js'
        else:
            if callback.find('http') == -1:
                callback = 'http://' + callback
            if callback.find('web.js') == -1:
                callback = callback + '/web.js'

        self.render('promo.html',promo=promo,news=news,callback=callback)


class SummaryClickCountHandler(BaseHandler):
    def post(self):
        success = False
        message = None
        sid = self.get_argument('sid',None)
        if sid:
            try:
                # summary = self.db.query(SummaryClickCounter).filter(SummaryClickCounter.sid == sid).first()
                # if summary:
                #     print '===========\nyes'
                #     summary.clickTimes += 1
                # else:
                #     summary = SummaryClickCounter(sid=sid,clickTimes=1)
                #     self.db.add(summary)
                #     print '=============\noops!'
                # self.db.commit()
                # success = True

                self.db.query(Summary).filter(Summary.id == sid).update({
                    Summary.clickTimes : Summary.clickTimes + 1
                    })
                self.db.commit()
                success = True
            except Exception,e:
                logging.error(e)
        self.resp_message(success=success,message=sid)



