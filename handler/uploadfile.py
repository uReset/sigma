#!/usr/bin/env python
#coding:utf-8


import os
import time
import logging

import tornado.web

from base import BaseHandler
from lib.models  import Custom,City,Zone,Promo,Tag
from lib.helpers import save_img,generate_ymd




class UploadFileHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        citys = self.db.query(City).order_by(City.id.asc()).all()
        zones = self.db.query(Zone).filter_by(city_code=citys[0].code).order_by(Zone.id.asc()).all()
        tags  = self.db.query(Tag).order_by(Tag.id.asc()).all()
        self.render('backend_uploads.html',citys=citys,zones=zones,tags=tags)

    @tornado.web.authenticated
    def post(self):
        if self.get_argument('tag',None) == u'promo':
            city_code   = self.get_argument('city_code',None)
            zone_code   = self.get_argument('zone_code',None)
            tag_code    = self.get_argument('tag_code',None)
            upload_file = self.request.files['myfile'][0]
            file_name   = upload_file['filename']
            if (city_code and zone_code and tag_code and upload_file):
                year,mon,day = generate_ymd()
                path      = os.path.join('include','promo',city_code,zone_code,tag_code,year,mon,day)
                img_url   = os.path.join(path,file_name)
                file_path = os.path.join(os.path.abspath('.'),self.application.settings['static_path'],path)
                save_img(
                    file_path   = file_path,
                    upload_file = upload_file
                    )
                promo_id = city_code + zone_code + tag_code
                promo    = self.db.query(Promo).filter_by(promo_id=promo_id).first()
                if promo:
                    promo.last_url = img_url
                    self.db.commit()
                else:
                    promo = Promo(
                        promo_id  = promo_id,
                        city_code = city_code,
                        zone_code = zone_code,
                        tag_code  = tag_code,
                        last_url  = img_url
                    )
                    self.db.add(promo)
                    self.db.commit()
                self.write('UploadHandler post')
                # self.redirect(img_url)
        else:
            self.redirect(self.reverse_url('uploads'))