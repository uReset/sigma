#!/usr/bin/env python
#coding:utf-8

import os
import logging

import tornado.web
import tornado.escape


from lib.filters import Filters
from lib.helpers import Config
from lib.models  import DB_Session,User
from config      import Site_Info


Sigma_Site_Info = Config()
Sigma_Site_Info.from_object(Site_Info)



class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, *argc, **argkw):
        super(BaseHandler, self).__init__(*argc, **argkw)
        # self.db_session = lib.session.Session(self.application.session_manager, self)
        self.jinja2 = self.settings.get("jinja2")
        self.jinja2 = Filters(self.jinja2).register()
        self.jinja2.globals.update(self.get_template_namespace())
        # self.jinja2.globals.update(Sigma_Site_Info)
        # self.db = DB_Session()

    # def initialize(self):
    #     # self.db_session = DB_Session()
    #     self.db = DB_Session()

    def on_finish(self):
        self.db.close()


    @property
    def db(self):
        return self.application.db

    @property
    def redis(self):
        return self.application.db_session


    def get(self):
        self.send_error(status_code=405)


    def post(self):
        self.send_error(status_code=405)


    def render(self, template_name, **template_vars):
        template_vars.update(Sigma_Site_Info)
        html = self.render_string(template_name, **template_vars)
        # self.write(html)
        self.finish(html)


    def render_string(self, template_name, **template_vars):
        # template_vars["xsrf_form_html"] = self.xsrf_form_html
        # template_vars["current_user"] = self.current_user
        # template_vars["request"] = self.request
        # template_vars["request_handler"] = self
        template = self.jinja2.get_template(template_name)
        return template.render(**template_vars)


    def render_from_string(self, template_string, **template_vars):
        template = self.jinja2.from_string(template_string)
        return template.render(**template_vars)


    def flash(self,message,category='message'):
        messages = self.messages(with_categories=True)
        messages.append((category, message))
        self.set_secure_cookie('flash_msg',tornado.escape.json_encode(messages))


    def get_flashed_messages(self,with_categories=False, category_filter=[]):
        messages = self.messages(with_categories,category_filter)
        if messages:
            self.clear_cookie('flash_msg')
        return messages


    def messages(self,with_categories=False, category_filter=[]):
        messages = self.get_secure_cookie('flash_msg')
        messages = tornado.escape.json_decode(messages) if messages else []
        if messages:
            if category_filter:
                messages = list(filter(lambda f: f[0] in category_filter, messages))
            if not with_categories:
                return [x[1] for x in messages]
        return messages


    def resp_message(self,success=False,**kwargs):
        #返回处理结果 True False
        resp = dict(success = success)
        resp.update(kwargs)
        self.set_header("Content-Type", "application/json")
        self.write(tornado.escape.json_encode(resp))


    # def flash(self, message, category='message'):
    #     messages = self.messages()
    #     messages.append((category, message))
    #     self.set_secure_cookie('flash_messages', tornado.escape.json_encode(messages))
    
    # def messages(self):
    #     messages = self.get_secure_cookie('flash_messages')
    #     messages = tornado.escape.json_decode(messages) if messages else []
    #     return messages
        
    # def get_flashed_messages(self):
    #     messages = self.messages()
    #     self.clear_cookie('flash_messages')
    #     return messages


    def template_namespace(self):
        template_namespace = dict(
            messages = self.messages
        )
        template_namespace.update(self.get_template_namespace())
        return template_namespace