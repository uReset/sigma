/*
SQLyog Community v11.25 (64 bit)
MySQL - 5.5.33a-MariaDB-log : Database - sigma
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sigma` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sigma`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(12) NOT NULL,
  `name` char(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_city` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `custom` */

DROP TABLE IF EXISTS `custom`;

CREATE TABLE `custom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_id` char(12) NOT NULL,
  `custom_name` char(15) NOT NULL,
  `tag_code` char(12) NOT NULL,
  `tag_name` char(15) NOT NULL,
  `city_code` char(12) NOT NULL,
  `city_name` char(15) NOT NULL,
  `zone_code` char(12) NOT NULL,
  `zone_name` char(15) NOT NULL,
  `last_url` char(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_custom` (`custom_id`,`last_url`),
  KEY `fc_city_code` (`city_code`),
  KEY `fc_zone_code` (`zone_code`),
  KEY `fc_tag_code` (`tag_code`),
  CONSTRAINT `fc_zone_code` FOREIGN KEY (`zone_code`) REFERENCES `zone` (`code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fc_tag_code` FOREIGN KEY (`tag_code`) REFERENCES `tag` (`code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fc_city_code` FOREIGN KEY (`city_code`) REFERENCES `city` (`code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tag` */

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(12) NOT NULL,
  `name` char(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_tag_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(16) NOT NULL,
  `passwd` char(32) NOT NULL,
  `email` char(32) DEFAULT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_user` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `zone` */

DROP TABLE IF EXISTS `zone`;

CREATE TABLE `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(12) NOT NULL,
  `name` char(15) NOT NULL,
  `city_code` char(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_zone` (`code`),
  KEY `fc_zone_city_code` (`city_code`),
  CONSTRAINT `fc_zone_city_code` FOREIGN KEY (`city_code`) REFERENCES `city` (`code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
