#!/usr/bin/env python
#coding:utf-8

import redis
import time
import datetime



class Session(object):
    pass




class RedisSession(redis.StrictRedis):
    def __init__(self,host,port,db):
        # self.r = redis.StrictRedis(host=host,port=port,db=db)
        super(RedisSession,self).__init__(host,port,db)


    def authlog(self,methods = '',custom_code = ''):
        authlog  = 'authlog:%s' % datetime.date.today()


        self.hincrby(authlog,'%s:%s:%s' % (custom_code,methods,datetime.date.today()) , 1)
        if self.ttl(authlog) < 0:
            hours,minutes,seconds = time.localtime()[3:6]
            diff_sec       = datetime.timedelta(
                                hours   = hours,
                                minutes = minutes,
                                seconds = seconds).total_seconds()
            diff_sec       = int(diff_sec)
            expire_seconds = 604800 - diff_sec
            self.expire(authlog,expire_seconds)


    def get_analysis(self,custom_list,query_date):
        date   = datetime.date.today() if not query_date else query_date
        c_logs = []
        for custom in custom_list:
            custom_name = self.get('custom:%s:name' % custom)
            auth,promo  = self.hmget(
                'authlog:%s' % date,
                '%s:auth:%s' % (custom,date),
                '%s:promo:%s' %(custom,date)
                )
            log = {
                'custom_code':custom,
                'custom_name':custom_name,
                'auth_times':int(auth) if auth else 0,
                'promo_times':int(promo) if promo else 0
                }
            c_logs.append(log)
        return c_logs

    def update_custom(self,code=None,name=None,action=u'update'):
        '''action:"update", "add" or "delete" '''

        if action == u'add':
            self.rpush('custom_code',code)
            self.set('custom:%s:name' % code,name)
        elif action == u'delete':
            self.lrem('custom_code',0,code)
            # Custom remove but data about auth times need keep alive
            # self.delete('custom:%s:name' % code)
        else:
            self.set('custom:%s:name' % code,name)





def main():
    red = RedisSession(host='127.0.0.1',port=63790,db=0)
    red.authlog(methods='test',custom_code='6379')
    red.set('test','0')




if __name__ == '__main__':
    main()