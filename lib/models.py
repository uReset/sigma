#!/usr/bin/env python
#coding:utf-8


import datetime

from sqlalchemy                 import exc,event,create_engine,Column,ForeignKey
from sqlalchemy.pool            import Pool
from sqlalchemy.orm             import sessionmaker,scoped_session
from sqlalchemy.types           import VARCHAR,CHAR,Integer,String,Boolean,DateTime,TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base

from lib.helpers import Config
from config import DB_Config


@event.listens_for(Pool, "checkout")
def ping_connection(dbapi_connection, connection_record, connection_proxy):
    cursor = dbapi_connection.cursor()
    try:
        cursor.execute("SELECT 1")
    except:
        # optional - dispose the whole pool
        # instead of invalidating one at a time
        # connection_proxy._pool.dispose()

        # raise DisconnectionError - pool will try
        # connecting again up to three times before raising.
        raise exc.DisconnectionError()
    cursor.close()


Sigma_DB_Config = Config()
Sigma_DB_Config.from_object(DB_Config)
# print Sigma_DB_Config.DB_CONNECT_STRING

BaseModel         = declarative_base()
# engine            = create_engine(Sigma_DB_Config.DB_CONNECT_STRING, echo=True)
# DB_Session        = sessionmaker(bind=engine)

def engine(echo=False):
    return create_engine(Sigma_DB_Config.DB_CONNECT_STRING, echo=echo)
DB_Session        = sessionmaker(bind=engine())

session           = DB_Session()


def init_db():
    # BaseModel.metadata.create_all(engine)
    BaseModel.metadata.create_all(engine())

def drop_db():
    # BaseModel.metadata.drop_all(engine)
    BaseModel.metadata.drop_all(engine())


class Custom(BaseModel):
    __tablename__  = 'custom'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
        }

    id          = Column(Integer,primary_key=True)
    custom_code = Column(CHAR(12),nullable=False,unique=True)
    custom_name = Column(CHAR(15),nullable=False)
    tag_code    = Column(CHAR(12),ForeignKey('tag.code',ondelete='No Action',onupdate='CASCADE'),nullable=False)
    # tag_name    = Column(CHAR(15),nullable=False)
    city_code   = Column(CHAR(12),ForeignKey('city.code',ondelete='No Action',onupdate='CASCADE'),nullable=False)
    # city_name   = Column(CHAR(15),nullable=False)
    zone_code   = Column(CHAR(12),ForeignKey('zone.code',ondelete='No Action',onupdate='CASCADE'),nullable=False)
    # zone_name   = Column(CHAR(15),nullable=False)
    last_url    = Column(CHAR(128),nullable=True,unique=False)
    status      = Column(Boolean,nullable=False,default=True)

    def __repr__(self):
        return "<Custom %r>" % (self.custom_name)

    def to_json(self):
        return {
            'id'   : self.id,
            'code' : self.custom_code,
            'name' : self.custom_name
            }

    def to_dict(self):
            def convert_datetime(value):
                return value.strftime("%Y-%m-%d %H:%M:%S")
            d = {}
            for c in self.__table__.columns:
                if isinstance(c.type, DateTime):
                 value = convert_datetime(getattr(self, c.name))
                else:
                    value = getattr(self, c.name)
                d[c.name] = value
            return d



class Promo(BaseModel):
    __tablename__ = 'promo_img'

    id        = Column(Integer,primary_key=True,autoincrement=True)
    promo_id  = Column(CHAR(36),unique=True,nullable=False)
    city_code = Column(CHAR(12),ForeignKey('city.code',ondelete='No Action',onupdate='CASCADE'),nullable=False)
    zone_code = Column(CHAR(12),ForeignKey('zone.code',ondelete='No Action',onupdate='CASCADE'),nullable=False)
    tag_code  = Column(CHAR(12),nullable=False)
    last_url  = Column(CHAR(128),nullable=False,unique=True)

    def __repr(self):
        # return "<Promo city:%r zone:%r tag:%r>" % (self.city_code,self.zone_code,self.tag_code)
        return "<Promo URL %r>" % (self.last_url)


class City(BaseModel):
    __tablename__ = 'city'

    id   = Column(Integer,primary_key=True)
    code = Column(CHAR(12),nullable=False,unique=True)
    name = Column(CHAR(15),nullable=False)

    def __repr__(self):
        return "City <%r>" % (self.name)

    def to_json(self):
        return {
            'code':self.code,
            'name':self.name
            }


class Zone(BaseModel):
    __tablename__ = 'zone'

    id        = Column(Integer,primary_key=True)
    code      = Column(CHAR(12),nullable=False,unique=True)
    name      = Column(CHAR(15),nullable=False)
    city_code = Column(CHAR(12),ForeignKey('city.code',ondelete='No Action',onupdate='CASCADE'),nullable=False)

    def __repr__(self):
        return "Zone <%r>" % (self.name)

    def to_json(self):
        return {
            'code':self.code,
            'name':self.name
            }


class Tag(BaseModel):
    __tablename__ = 'tag'

    id   = Column(Integer,primary_key=True)
    code = Column(CHAR(12),nullable=False,unique=True)
    name = Column(CHAR(15),nullable=False)

    def __repr__(self):
        return "Tag <%r>" % (self.name)


class User(BaseModel):
    """docstring for Users"""
    __tablename__  = 'user'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
        }

    id     = Column(Integer,primary_key=True)
    name   = Column(CHAR(16),nullable=False,unique=True)
    passwd = Column(CHAR(32),nullable=False)
    email  = Column(CHAR(24),nullable=False,unique=True)
    avatar = Column(CHAR(128),nullable=False,unique=True)
    role   = Column(Boolean(),default=True)

    def __repr__(self):
        return "User <%r>" % (self.name)




class AuthLog(BaseModel):
    __tablename__ = 'auth_log'

    id          = Column(Integer,primary_key=True)
    custom_code = Column(CHAR(12),nullable=False)
    methods     = Column(CHAR(6),nullable=False)    # 'auth' or 'promo'
    ip_addrs    = Column(CHAR(15),nullable=False)
    UA          = Column(VARCHAR(256),nullable=False)
    log_time    = Column(TIMESTAMP(timezone=False),nullable=False,default=datetime.datetime.now())




class Token(BaseModel):
    __tablename__ = 'token'

    id          = Column(Integer,primary_key=True)
    username    = Column(CHAR(24),nullable=False)
    token       = Column(CHAR(128),nullable=False)
    expire_time = Column(TIMESTAMP(timezone=False),default=datetime.datetime.now())




class Summary(BaseModel):
    __tablename__ = 'summary'

    id              = Column(Integer,primary_key=True)
    title           = Column(CHAR(32),nullable=False)
    source          = Column(CHAR(32),nullable=False) 
    link            = Column(CHAR(128),nullable=False,unique=True)
    create_Time     = Column(TIMESTAMP(timezone=False),nullable=False,default=datetime.datetime.now())
    # lastUpdateTime  = Column(TIMESTAMP(timezone=False),nullable=False,default=datetime.datetime.now())
    active          = Column(Boolean,nullable=False,default=True)
    deleted         = Column(Boolean,nullable=False,default=False)
    clickTimes      = Column(Integer,default=0,nullable=False)




class SummaryClickCounter(BaseModel):
    __tablename__ = 'summary_click_counter'

    id         = Column(Integer,primary_key=True)
    sid        = Column(Integer,ForeignKey('summary.id',ondelete='No Action',onupdate='CASCADE'),nullable=False)
    clickTimes = Column(Integer,default=0,nullable=False)