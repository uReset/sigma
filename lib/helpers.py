#!/usr/bin/env python
#coding:utf-8


import os
import time
import imp
import logging
import json

from threading import Thread
from sqlalchemy.ext.declarative import DeclarativeMeta
from config import File_Upload_Config


# class AlchemyEncoder(json.JSONEncoder):
#     def default(self, obj):
#         if isinstance(obj.__class__, DeclarativeMeta):
#             # an SQLAlchemy class
#             fields = {}
#             for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
#                 data = obj.__getattribute__(field)
#                 try:
#                     json.dumps(data) # this will fail on non-encodable values, like other classes
#                     fields[field] = data
#                 except TypeError:
#                     fields[field] = None
#             # a json-encodable dict
#             return fields
#         return json.JSONEncoder.default(self, obj)


def new_alchemy_encoder(revisit_self = False, fields_to_expand = [],ignore=[]):
    _visited_objs = []
    # you can ignore some fileds if you need
    ignores = ['metadata']
    ignores += ignore
    class AlchemyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj.__class__, DeclarativeMeta):
                # don't re-visit self
                if revisit_self:
                    if obj in _visited_objs:
                        return None
                    _visited_objs.append(obj)

                # go through each field in this SQLalchemy class
                fields = {}
                for field in [x for x in dir(obj) if not x.startswith('_') and x not in ignores]:
                    val = obj.__getattribute__(field)

                    # is this field another SQLalchemy object, or a list of SQLalchemy objects?
                    if isinstance(val.__class__, DeclarativeMeta) or (isinstance(val, list) and len(val) > 0 and isinstance(val[0].__class__, DeclarativeMeta)):
                        # unless we're expanding this field, stop here
                        if field not in fields_to_expand:
                            # not expanding this field: set it to None and continue
                            fields[field] = None
                            continue

                    fields[field] = val
                # a json-encodable dict
                return fields

            return json.JSONEncoder.default(self, obj)
    return AlchemyEncoder



class Config(dict):
    '''Config like flask.config'''
    # def __init__(self):
    #     dict.__init__(self)
    #     self.root_path = root_path

    def from_object(self,obj):
        for key in dir(obj):
            if key.isupper():
                self[key] = getattr(obj,key)


    def from_pyfile(self,file_path=None,silent=False):
        filename = file_path
        d = imp.new_module('config')
        d.__file__ = filename
        try:
            with open(filename) as config_file:
                exec(compile(config_file.read(), filename, 'exec'), d.__dict__)
        except IOError as e:
            if silent and e.errno in (errno.ENOENT, errno.EISDIR):
                return False
            e.strerror = 'Unable to load configuration file (%s)' % e.strerror
            raise
        self.from_object(d)
        return True

    # def __repr__(self):
    #     return '<%s %s>' % (self.__class__.__name__, dict.__repr__(self))


    def __getattr__(self,name):
        '''with this you can use dot syntax'''
        return self[name]




def async(f):
    def warpper(*args,**kwargs):
        thr = Thread(target = f, args = args, kwargs = kwargs)
        thr.start()
    return warpper

def check_extensions(filename):
    settings = Config()
    settings.from_object(File_Upload_Config)
    if '.' in filename and filename.split('.')[-1].lower() in settings['ALLOWED_EXTENSIONS']:
        return True
    else:
        return False


@async
def save_img(file_path,upload_file):
    if not os.path.isdir(file_path):
        logging.info('The directory %r does not exist,will be created ' % file_path)
        try:
            os.system('mkdir -m 755 -p %s' % file_path)
            logging.info('Created the directory %r' % file_path)
        except Exception,e:
            logging.error(e)
    filename = upload_file['filename']
    with open(os.path.join(file_path,filename),'wb') as f:
        try:
            f.write(upload_file['body'])
            f.close()
            logging.info('Saved file %r to %r' % (filename,file_path))
        except Exception,e:
            logging.error(e)
    return True


def save_file(path,name,upfile):
    # file_path = os.path.join(os.path.abspath('.'),self.application.settings['static_path'],path)
    if not os.path.isdir(path):
        logging.info('The directory %r does not exist,will be created ' % path)
        try:
            os.mkdir(path)
            logging.info('Created the directory %r' % path)
        except Exception,e:
            logging.error(e)
            return False
    with open(os.path.join(path,name),'wb') as f:
        try:
            f.write(upfile['body'])
            f.close()
            logging.info('Saved file %r to %r' % (name,path))
            return True
        except Exception,e:
            logging.error(e)
            return False


def generate_ymd():
    tp = time.localtime()[:3]
    li = list(tp)
    for i in range(len(li)):
        li[i] = "%02d" % li[i]
    return li




if __name__ == '__main__':
    config = Config()
    path = os.path.join(os.path.abspath('..'),'config.py')
    config.from_pyfile(path)
    print config.__repr__()
