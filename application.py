#!/usr/bin/env python
#coding:utf-8

import os
import torndb
import logging
import redis

import tornado.web
import tornado.ioloop
from tornado.options import options,define

from jinja2 import Environment, FileSystemLoader

from sqlalchemy.orm import scoped_session, sessionmaker

import handler.front
# import handler.login
import handler.uploadfile
import handler.manager
# import handler.uimodules
from lib.session import RedisSession
from lib.helpers import Config
from lib.models  import engine
from config      import App_Config,Redis_Config



Site_Config = Config()
Site_Config.from_object(App_Config)

r_conf = Config()
r_conf.from_object(Redis_Config)

define('port',default=8000,type=int,help='listen port')
define('address',default="127.0.0.1",type=str,help="Server Bind IP_address")
# define('SITE_NAME',default=u'Your Site Name')
# define('DB_NAME',default=os.path.join(os.path.abspath('.'),'config.py'))

# # tornado.options.parse_config_file('config.py')




class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            jinja2        = Environment(loader = FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates")), 
                                        trim_blocks = True
                                        ),
            template_path = os.path.join(os.path.dirname(__file__),'templates'),
            static_path   = os.path.join(os.path.dirname(__file__),'static'),
            # ui_modules    = handler.uimodules,
            cookie_secret = Site_Config.COOKIE_SECRET,
            # xsrf_cookies  = True,
            login_url     = '/backend/login',
            gzip          = True,
            debug         = Site_Config.DEBUG,)
        handlers = [
            (r'/robots.txt',tornado.web.StaticFileHandler, dict(path=settings['static_path'])),
            (r'/favicon.ico',tornado.web.StaticFileHandler, dict(path=settings['static_path'])),
            tornado.web.url(r'/',handler.front.IndexHandler,name='index'),
            tornado.web.url(r'/auth',handler.front.CustomHandler,name='custom_auth'),
            tornado.web.url(r'/promo',handler.front.PromoHandler,name='promo'),
            tornado.web.url(r'/summary/counter',handler.front.SummaryClickCountHandler,name='click_counter')
        ]

        tornado.web.Application.__init__(self,handlers,**settings)
        # self.db = torndb.Connection(
        #     host     = '127.0.0.1:33060',
        #     database = 'sigma',
        #     user     = 'root',
        #     password = 'lneoe'
        #     )
        self.db = scoped_session(sessionmaker(bind=engine(echo=settings['debug'])))
        self.db_session = RedisSession(host=r_conf.HOST,port=r_conf.PORT,db=r_conf.DB)

def main():
    # import logging.config
    # import logging.handlers
    # import yaml
    # logging.config.dictConfig(yaml.load(open('logging.yaml', 'r')))
    # import json
    # logging.config.dictConfig(json.load(open('logging.json','rb')))
    tornado.options.parse_command_line()
    app = Application()
    app.listen(port=options.port,address=options.address,xheaders=True)
    logging.warn('Server start at %s:%r' % (options.address,options.port))
    tornado.ioloop.IOLoop.instance().start()




if __name__ == '__main__':
    main()
