#!/usr/bin/env python
#coding:utf-8

import os
import sys
import torndb
import logging
import redis

import tornado.web
import tornado.ioloop
from tornado.options import options,define

from jinja2 import Environment, FileSystemLoader

from sqlalchemy.orm import scoped_session, sessionmaker

import handler.front
# import handler.login
import handler.uploadfile
import handler.manager
# import handler.uimodules
from lib.session import RedisSession
from lib.helpers import Config
from lib.models  import engine,Custom
from config      import App_Config,Redis_Config



Site_Config = Config()
Site_Config.from_object(App_Config)

r_conf = Config()
r_conf.from_object(Redis_Config)

define('port',default=8000,type=int,help='listen port')
define('address',default="127.0.0.1",type=str,help="Server Bind IP_address")
# define('SITE_NAME',default=u'Your Site Name')
# define('DB_NAME',default=os.path.join(os.path.abspath('.'),'config.py'))

# # tornado.options.parse_config_file('config.py')




class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            jinja2        = Environment(loader = FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates")), 
                                        trim_blocks = True
                                        ),
            template_path = os.path.join(os.path.dirname(__file__),'templates'),
            static_path   = os.path.join(os.path.dirname(__file__),'static'),
            # ui_modules    = handler.uimodules,
            cookie_secret = Site_Config.COOKIE_SECRET,
            # xsrf_cookies  = True,
            login_url     = '/backend/login',
            gzip          = True,
            debug         = Site_Config.DEBUG,)
        handlers = [
            (r'/robots.txt',tornado.web.StaticFileHandler, dict(path=settings['static_path'])),
            (r'/favicon.ico',tornado.web.StaticFileHandler, dict(path=settings['static_path'])),
            (r'/backend/zone_list',handler.manager.ZoneListHandler),
            (r'/backend/custom_list',handler.manager.CustomListHandler),
            (r'/backend/get_custom/*',handler.manager.CustomInfoHandler),
            (r'/backend/filter',handler.manager.UAFilterListHandler),
            tornado.web.url(r'/',handler.front.IndexHandler,name='index'),
            tornado.web.url(r'/backend',handler.manager.ManagerIndexHandler,name='backend'),
            tornado.web.url(r'/backend/login',handler.manager.LoginHandler,name='login'),
            tornado.web.url(r'/backend/logout',handler.manager.LogoutHandler,name='logout'),
            tornado.web.url(r'/backend/custom-manager$',handler.manager.CustomManagerHandler,name='custom_manager'),
            tornado.web.url(r'/backend/custom-manager/(\d{1,6}$)',handler.manager.CustomManagerHandler,name='custom_manager'),
            tornado.web.url(r'/backend/zone-img-update',handler.manager.ZoneImgUpdateHandler,name='zone_img_update'),
            tornado.web.url(r'/backend/uploads',handler.manager.UploadFileHandler,name='uploads'),
            tornado.web.url(r'/backend/analysis',handler.manager.AnalysisHandler,name='analysis'),
            tornado.web.url(r'/backend/recommend',handler.manager.ManagerSummaryHandler,name='manager_recommend')
        ]

        tornado.web.Application.__init__(self,handlers,**settings)
        # self.db = torndb.Connection(
        #     host     = '127.0.0.1:33060',
        #     database = 'sigma',
        #     user     = 'root',
        #     password = 'lneoe'
        #     )
        self.db = scoped_session(sessionmaker(bind=engine(echo=settings['debug'])))
        self.db_session = RedisSession(host=r_conf.HOST,port=r_conf.PORT,db=r_conf.DB)

def main():
    # import logging.config
    # import logging.handlers
    # import yaml
    # logging.config.dictConfig(yaml.load(open('logging.yaml', 'r')))
    # import json
    # logging.config.dictConfig(json.load(open('logging.json','rb')))
    tornado.options.parse_command_line()
    app = Application()
    try:
        # is_custom = app.db_session.keys(u'custom:*:name')
        # if is_custom:
        #     logging.info(u'商户列表已存在,执行下一步...')
        #     pass
        # else:
        #     logging.info(u'开始初始化商户列表...')
        #     custom_name = app.db().bind.execute('''
        #         select custom.custom_code,custom.custom_name
        #         from custom
        #         where custom.status=1
        #         order by custom.custom_code asc
        #         ''')
        #     if custom_name:
        #         for code,name in custom_name:
        #             app.db_session.rpush('custom_code',code)
        #             app.db_session.set('custom:%s:name' % code,name)
        #         logging.info(u'初始化商户列表成功!')
        #     else:
        #         logging.warn(u'初始化商户列表失败，服务继续启动...\n请选择手动修复!')
        
        logging.info(u'开始初始化商户列表...')
        custom_name = app.db().bind.execute('''
            select custom.custom_code,custom.custom_name
            from custom
            where custom.status=1
            order by custom.custom_code asc
            ''')
        if custom_name:
            app.db_session.delete('custom_code')
            for code,name in custom_name:
                app.db_session.rpush('custom_code',code)
                app.db_session.set('custom:%s:name' % code,name)
            logging.info(u'初始化商户列表成功!')
        else:
            logging.warn(u'初始化商户列表失败，服务继续启动...\n请选择手动修复!')
    except Exception,e:
        logging.error(e)
        logging.error(u'启动时出错了，启动终止!')
        sys.exit()
    app.listen(port=options.port,address=options.address,xheaders=True)
    logging.warn('Server start at %s:%r' % (options.address,options.port))
    tornado.ioloop.IOLoop.instance().start()




if __name__ == '__main__':
    main()
