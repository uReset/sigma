/*
*jQuery插件，获取url参数
*/

(function($){
    $.getUrlParam = function(name){
        var result = new RegExp(name + "=([^&]*)", "i").exec(window.location.search); 
        return result && unescape(result[1]) || ""; 
    };
})(jQuery);



/*
 * Copyright (c) 2013 Mike King (@micjamking)
 *
 * jQuery Succinct plugin
 * Version 1.0.1 (July 2013)
 *
 * Licensed under the MIT License
 */

(function(a){a.fn.succinct=function(b){var c={size:240,omission:"...",ignore:true},b=a.extend(c,b);return this.each(function(){var e,d,h=a(this),g=/[!-\/:-@\[-`{-~]$/;var f=function(){h.each(function(){e=a(this).text();if(e.length>b.size){d=a.trim(e).substring(0,b.size).split(" ").slice(0,-1).join(" ");if(b.ignore){d=d.replace(g,"")}a(this).text(d+b.omission)}})};var i=function(){f()};i()})}})(jQuery);





/*
 * jQuery MD5 Plugin 1.2.1
 * https://github.com/blueimp/jQuery-MD5
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 * 
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/*jslint bitwise: true */
/*global unescape, jQuery */

(function ($) {
    'use strict';

    /*
    * Add integers, wrapping at 2^32. This uses 16-bit operations internally
    * to work around bugs in some JS interpreters.
    */
    function safe_add(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF),
            msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    /*
    * Bitwise rotate a 32-bit number to the left.
    */
    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    /*
    * These functions implement the four basic operations the algorithm uses.
    */
    function md5_cmn(q, a, b, x, s, t) {
        return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
    }
    function md5_ff(a, b, c, d, x, s, t) {
        return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }
    function md5_gg(a, b, c, d, x, s, t) {
        return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }
    function md5_hh(a, b, c, d, x, s, t) {
        return md5_cmn(b ^ c ^ d, a, b, x, s, t);
    }
    function md5_ii(a, b, c, d, x, s, t) {
        return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
    }

    /*
    * Calculate the MD5 of an array of little-endian words, and a bit length.
    */
    function binl_md5(x, len) {
        /* append padding */
        x[len >> 5] |= 0x80 << ((len) % 32);
        x[(((len + 64) >>> 9) << 4) + 14] = len;

        var i, olda, oldb, oldc, oldd,
            a =  1732584193,
            b = -271733879,
            c = -1732584194,
            d =  271733878;

        for (i = 0; i < x.length; i += 16) {
            olda = a;
            oldb = b;
            oldc = c;
            oldd = d;

            a = md5_ff(a, b, c, d, x[i],       7, -680876936);
            d = md5_ff(d, a, b, c, x[i +  1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i +  2], 17,  606105819);
            b = md5_ff(b, c, d, a, x[i +  3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i +  4],  7, -176418897);
            d = md5_ff(d, a, b, c, x[i +  5], 12,  1200080426);
            c = md5_ff(c, d, a, b, x[i +  6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i +  7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i +  8],  7,  1770035416);
            d = md5_ff(d, a, b, c, x[i +  9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i + 12],  7,  1804603682);
            d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i + 15], 22,  1236535329);

            a = md5_gg(a, b, c, d, x[i +  1],  5, -165796510);
            d = md5_gg(d, a, b, c, x[i +  6],  9, -1069501632);
            c = md5_gg(c, d, a, b, x[i + 11], 14,  643717713);
            b = md5_gg(b, c, d, a, x[i],      20, -373897302);
            a = md5_gg(a, b, c, d, x[i +  5],  5, -701558691);
            d = md5_gg(d, a, b, c, x[i + 10],  9,  38016083);
            c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i +  4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i +  9],  5,  568446438);
            d = md5_gg(d, a, b, c, x[i + 14],  9, -1019803690);
            c = md5_gg(c, d, a, b, x[i +  3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i +  8], 20,  1163531501);
            a = md5_gg(a, b, c, d, x[i + 13],  5, -1444681467);
            d = md5_gg(d, a, b, c, x[i +  2],  9, -51403784);
            c = md5_gg(c, d, a, b, x[i +  7], 14,  1735328473);
            b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

            a = md5_hh(a, b, c, d, x[i +  5],  4, -378558);
            d = md5_hh(d, a, b, c, x[i +  8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i + 11], 16,  1839030562);
            b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i +  1],  4, -1530992060);
            d = md5_hh(d, a, b, c, x[i +  4], 11,  1272893353);
            c = md5_hh(c, d, a, b, x[i +  7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i + 13],  4,  681279174);
            d = md5_hh(d, a, b, c, x[i],      11, -358537222);
            c = md5_hh(c, d, a, b, x[i +  3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i +  6], 23,  76029189);
            a = md5_hh(a, b, c, d, x[i +  9],  4, -640364487);
            d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i + 15], 16,  530742520);
            b = md5_hh(b, c, d, a, x[i +  2], 23, -995338651);

            a = md5_ii(a, b, c, d, x[i],       6, -198630844);
            d = md5_ii(d, a, b, c, x[i +  7], 10,  1126891415);
            c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i +  5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i + 12],  6,  1700485571);
            d = md5_ii(d, a, b, c, x[i +  3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i +  1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i +  8],  6,  1873313359);
            d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i +  6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i + 13], 21,  1309151649);
            a = md5_ii(a, b, c, d, x[i +  4],  6, -145523070);
            d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i +  2], 15,  718787259);
            b = md5_ii(b, c, d, a, x[i +  9], 21, -343485551);

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
        }
        return [a, b, c, d];
    }

    /*
    * Convert an array of little-endian words to a string
    */
    function binl2rstr(input) {
        var i,
            output = '';
        for (i = 0; i < input.length * 32; i += 8) {
            output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
        }
        return output;
    }

    /*
    * Convert a raw string to an array of little-endian words
    * Characters >255 have their high-byte silently ignored.
    */
    function rstr2binl(input) {
        var i,
            output = [];
        output[(input.length >> 2) - 1] = undefined;
        for (i = 0; i < output.length; i += 1) {
            output[i] = 0;
        }
        for (i = 0; i < input.length * 8; i += 8) {
            output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
        }
        return output;
    }

    /*
    * Calculate the MD5 of a raw string
    */
    function rstr_md5(s) {
        return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
    }

    /*
    * Calculate the HMAC-MD5, of a key and some data (raw strings)
    */
    function rstr_hmac_md5(key, data) {
        var i,
            bkey = rstr2binl(key),
            ipad = [],
            opad = [],
            hash;
        ipad[15] = opad[15] = undefined;                        
        if (bkey.length > 16) {
            bkey = binl_md5(bkey, key.length * 8);
        }
        for (i = 0; i < 16; i += 1) {
            ipad[i] = bkey[i] ^ 0x36363636;
            opad[i] = bkey[i] ^ 0x5C5C5C5C;
        }
        hash = binl_md5(ipad.concat(rstr2binl(data)), 512 + data.length * 8);
        return binl2rstr(binl_md5(opad.concat(hash), 512 + 128));
    }

    /*
    * Convert a raw string to a hex string
    */
    function rstr2hex(input) {
        var hex_tab = '0123456789abcdef',
            output = '',
            x,
            i;
        for (i = 0; i < input.length; i += 1) {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F) +
                hex_tab.charAt(x & 0x0F);
        }
        return output;
    }

    /*
    * Encode a string as utf-8
    */
    function str2rstr_utf8(input) {
        return unescape(encodeURIComponent(input));
    }

    /*
    * Take string arguments and return either raw or hex encoded strings
    */
    function raw_md5(s) {
        return rstr_md5(str2rstr_utf8(s));
    }
    function hex_md5(s) {
        return rstr2hex(raw_md5(s));
    }
    function raw_hmac_md5(k, d) {
        return rstr_hmac_md5(str2rstr_utf8(k), str2rstr_utf8(d));
    }
    function hex_hmac_md5(k, d) {
        return rstr2hex(raw_hmac_md5(k, d));
    }
    
    $.md5 = function (string, key, raw) {
        if (!key) {
            if (!raw) {
                return hex_md5(string);
            } else {
                return raw_md5(string);
            }
        }
        if (!raw) {
            return hex_hmac_md5(key, string);
        } else {
            return raw_hmac_md5(key, string);
        }
    };
    
}(typeof jQuery === 'function' ? jQuery : this));





/*! jquery-html5Validate.js 基于HTML5表单验证的jQuery插件
 * 支持type="email", type="number", type="tel", type="url", type="zipcode", 以及多type, 如type="email|tel". 支持 step, min, max, required, pattern, multiple
 * 有4个自定义扩展属性 data-key, data-target, data-min, data-max
 * 文档：http://www.zhangxinxu.com/wordpress/?p=2857
 * 如果您有任何问题，可以邮件至zhangxinxu@zhangxinxu.com
 * create by zhangxinxu 2012-12-05   
 * v1.0 beta
        on 2012-12-05 创建
        on 2012-12-06 兼容性调试
        on 2012-12-14 增加对multiple属性支持
                      testRemind方法的最大宽度限制
        on 2012-12-17 增加submitEnabled参数
        on 2012-12-19 暴露testRemind方法的CSS参数
        on 2012-12-20 testRemind尖角设置overflow: hidden for IE6
    v1.0 publish on 2012-12-20
    v1.2 on 2012-12-21 尖角实际高度可能覆盖单复选框的问题，做了高度限制处理
    v1.2.1 on 2013-03-20 增加hidden提示时候的定位，以便在多屏下的时候提示可见
    v1.3 on 2013-06-19 新增validate参数，应对其他一些自定义的验证
**/
(function($, undefined) {
    // 全角半角转换
    DBC2SBC = function(str) {
        var result = '', i, code;
        for (i=0 ; i<str.length; i++) {
            code = str.charCodeAt(i);
            if (code >= 65281 && code <= 65373) {
                result += String.fromCharCode(str.charCodeAt(i) - 65248);
            } else if (code == 12288) {
                result += String.fromCharCode(str.charCodeAt(i) - 12288 + 32);
            } else {
                result += str.charAt(i);
            }
        }
        return result;
    };
    
    // 自定义提示隐藏、绑定事件以及解绑事件
    $.testRemind = (function() {
        var winWidth = $(window).width();
        var fnMouseDown = function(e) {
            if (!e || !e.target) return;
            if (e.target.id !== $.testRemind.id && $(e.target).parents("#" + $.testRemind.id).length === 0) {
                $.testRemind.hide();
            }   
        }, fnKeyDown = function(e) {
            if (!e || !e.target) return;
            if (e.target.tagName.toLowerCase() !== "body") {
                $.testRemind.hide();
            }
        }, funResize = function() {
            if (!$.testRemind.display) return;
            var nowWinWidth = $(window).width();
            if (Math.abs(winWidth - nowWinWidth) > 20) {
                $.testRemind.hide();
                winWidth = nowWinWidth;
            }
        };
        return {
            id: "validateRemind",
            display: false,
            css: {},
            hide: function() {
                $("#" + this.id).remove();
                this.display = false;
                $(document).unbind({
                    mousedown:  fnMouseDown,
                    keydown: fnKeyDown
                });
                $(window).unbind("resize", funResize);
            },
            bind: function() {
                $(document).bind({
                    mousedown:  fnMouseDown,
                    keydown: fnKeyDown
                });
                $(window).bind("resize", funResize);
            }
        };      
    })();
    
    // 全局对象，可扩展
    OBJREG = {
        EMAIL:"^[a-z0-9._%-]+@([a-z0-9-]+\\.)+[a-z]{2,4}$",
        NUMBER: "^\\-?\\d+(\\.\\d+)?$",
        URL:"^(http|https|ftp)\\:\\/\\/[a-z0-9\\-\\.]+\\.[a-z]{2,3}(:[a-z0-9]*)?\\/?([a-z0-9\\-\\._\\?\\,\\'\\/\\\\\\+&amp;%\\$#\\=~])*$",
        TEL:"^1\\d{10}$",
        ZIPCODE:"^\\d{6}$",
        "prompt": {
            radio: "请选择一个选项",
            checkbox: "如果要继续，请选中此框",
            "select": "请选择列表中的一项",
            email: "请输入电子邮件地址",
            url: "请输入网站地址",
            tel: "请输入手机号码",
            number: "请输入数值",
            date: "请输入日期",
            pattern: "内容格式不符合要求",
            empty: "请填写此字段",
            multiple: "多条数据使用逗号分隔"
        }   
    };
    
    $.html5Attr = function(ele, attr) {
        if (!ele || !attr) return undefined;
        // 为了向下兼容jQuery 1.4
        if (document.querySelector) {
            return $(ele).attr(attr);   
        } else {
            // IE6, 7
            var ret;
            ret = ele.getAttributeNode(attr);
            // Return undefined if nodeValue is empty string
            return ret && ret.nodeValue !== "" ? ret.nodeValue : undefined; 
        }   
    };
    $.html5Validate = (function() { 
        // 验证需要的子集方法 如是否为空，是否正则匹配，是否溢出
        return {
            isSupport: (function() {
                return $('<input type="email">').attr("type") === "email";  
            })(),
            isEmpty: function(ele, value) {
                value = value || $.html5Attr(ele, "placeholder");
                var trimValue = ele.value;
                if (ele.type !== "password") {
                    trimValue = $.trim(trimValue);
                }
                if (trimValue === "" || trimValue === value) return true;   
                return false;   
            },
            isRegex: function(ele, regex, params) {
                // 原始值和处理值
                var inputValue = ele.value, dealValue = inputValue, type = ele.getAttribute("type") + "";
                type = type.replace(/\W+$/, "");
                
                if (type !== "password") {
                    // 密码不trim前后空格，以及全半角转换
                    dealValue = DBC2SBC($.trim(inputValue));
                    //  文本框值改变，重新赋值
                    dealValue !== inputValue && $(ele).val(dealValue);
                }
        
                // 获取正则表达式，pattern属性获取优先，然后通过type类型匹配。注意，不处理为空的情况
                regex = regex || (function() {
                    return $.html5Attr(ele, "pattern");
                })() || (function() {
                    // 文本框类型处理，可能有管道符——多类型重叠，如手机或邮箱
                    return type && $.map(type.split("|"), function(typeSplit) {
                        var matchRegex = OBJREG[typeSplit.toUpperCase()];
                        if (matchRegex) return matchRegex;
                    }).join("|");   
                })();
                
                if (dealValue === "" || !regex) return true;
                
                // multiple多数据的处理
                var isMultiple = $(ele).hasProp("multiple"), newRegExp = new RegExp(regex, params || 'i');
                // number类型下multiple是无效的
                if (isMultiple && !/^number|range$/i.test(type)) {
                    var isAllPass = true;
                    $.each(dealValue.split(","), function(i, partValue) {
                        partValue = $.trim(partValue);
                        if (isAllPass && !newRegExp.test(partValue)) {
                            isAllPass = false;
                        }
                    });
                    return isAllPass;
                } else {
                    return newRegExp.test(dealValue);   
                }
                return true;
            },
            isOverflow: function(ele) {
                if (!ele) return false;
                //  大小限制
                var attrMin = $(ele).attr("min"), attrMax = $(ele).attr("max"), attrStep
                    // 长度限制
                    , attrDataMin, attrDataMax
                    // 值
                    value = ele.value;
                    
                if (!attrMin && !attrMax) {
                    attrDataMin = $(ele).attr("data-min"), attrDataMax = $(ele).attr("data-max");
                    if (attrDataMin && value.length < attrDataMin) {
                        $(ele).testRemind("至少输入" + attrDataMin + "个字符");
                        ele.focus();
                    } else if (attrDataMax && value.length > attrDataMax) {
                        $(ele).testRemind("最多输入" + attrDataMax + "个字符");
                        $(ele).selectRange(attrDataMax, value.length);
                    } else {
                        return false;   
                    }
                } else {
                    // 数值大小限制
                    value = Number(value);
                    attrStep = Number($(ele).attr("step")) || 1;
                    if (attrMin && value < attrMin) {
                        $(ele).testRemind("值必须大于或等于" + attrMin);    
                    } else if (attrMax && value > attrMax) {
                        $(ele).testRemind("值必须小于或等于" + attrMax);    
                    } else if (attrStep && !/^\d+$/.test(Math.abs((value - attrMin || 0)) / attrStep)) {
                        $(ele).testRemind("值无效");   
                    } else {
                        return false;   
                    }
                    ele.focus();
                    ele.select();
                }
                return true;
            },
            isAllpass: function(elements, options) {
                if (!elements) return true;
                var defaults = {
                    // 优先label标签作为提示文字
                    labelDrive: true
                };
                params = $.extend({}, defaults, options || {});
                
                if (elements.size && elements.size() == 1 && elements.get(0).tagName.toLowerCase() == "form") {
                    elements = elements.find(":input"); 
                } else if (elements.tagName && elements.tagName.toLowerCase() == "form") {
                    elements = $(elements).find(":input");  
                }
                var self = this;
                var allpass = true
                  , remind = function(control, type, tag) {
                    var key = $(control).attr("data-key"), label = $("label[for='"+ control.id +"']"), text= '', placeholder;
                    
                    if (params.labelDrive) {
                        placeholder = $.html5Attr(control, "placeholder");
                        label.each(function() {
                            var txtLabel = $(this).text();
                            if (txtLabel !== placeholder) {
                                text += txtLabel.replace(/\*|:|：/g, "");
                            }
                        });
                    }
                    
                    // 如果元素完全显示
                    if ($(control).isVisible()) {
                        if (type == "radio" || type == "checkbox") {
                            $(control).testRemind(OBJREG.prompt[type], {
                                align: "left"   
                            });
                            control.focus();
                        } else if (tag == "select" || tag == "empty") {
                            // 下拉值为空或文本框文本域等为空
                            $(control).testRemind((tag == "empty" && text)? "您尚未输入"+ text : OBJREG.prompt[tag]);
                            control.focus();
                        } else if (/^range|number$/i.test(type) && Number(control.value)) {
                            // 整数值与数值的特殊提示
                            $(control).testRemind("值无效");
                            control.focus();
                            control.select();
                        } else {
                            // 文本框文本域格式不准确
                            // 提示文字的获取  
                            var finalText = OBJREG.prompt[type] || OBJREG.prompt["pattern"];
                            if (text) {
                                finalText = "您输入的"+ text +"格式不准确";
                            }
                            if (type != "number" && $(control).hasProp("multiple")) {
                                finalText += "，" + OBJREG.prompt["multiple"];
                            }
                            
                            $(control).testRemind(finalText);
                            control.focus();
                            control.select();   
                        }           
                    } else {
                        // 元素隐藏，寻找关联提示元素, 并走label提示流(radio, checkbox除外)
                        var selector = $(control).attr("data-target");
                        var target = $("#" + selector);
                        if (target.size() == 0) {
                            target = $("." + selector);
                        }
                        var customTxt = "您尚未" + (key || (tag == "empty"? "输入": "选择")) + ((!/^radio|checkbox$/i.test(type) && text) || "该项内容");
                        if (target.size()) {
                            if (target.offset().top < $(window).scrollTop()) {
                                $(window).scrollTop(target.offset().top - 50);
                            }
                            target.testRemind(customTxt);
                        } else {
                            alert(customTxt);   
                        }
                    }
                    return false;
                };
                
                elements.each(function(){
                    var el = this, type = el.getAttribute("type"), tag = el.tagName.toLowerCase(), isRequired = $(this).hasProp("required");
                    // type类型
                    if (type) {
                        var typeReplace = type.replace(/\W+$/, ""); 
                        if (!params.hasTypeNormally && $.html5Validate.isSupport && type != typeReplace) {
                            // 如果表单元素默认type类型保留，去除某位空格或管道符
                            try { el.type = typeReplace; } catch(e) {}
                        }
                        type = typeReplace;
                    }
                    
                    if (allpass == false || el.disabled || type == 'submit' || type == 'reset' || type == 'file' || type == 'image') return;
                    // 需要验证的有
                    // input文本框, type, required, pattern, max, min以及自定义个数限制data-min, data-max
                    // radio, checkbox
                    // select
                    // textarea
                    // 先从特殊的下手，如单复选框    
                    if (type == "radio" && isRequired) {
                        // 单选框，只需验证是否必选，同一name单选组只有要一个设置required即可
                        var eleRadios = el.name? $("input[type='radio'][name='"+ el.name +"']"): $(el)
                            , radiopass = false;
                            
                        eleRadios.each(function() {
                            if (radiopass == false && $(this).attr("checked")) {
                                radiopass = true;
                            }
                        });
                        
                        if (radiopass == false) {
                            allpass = remind(eleRadios.get(0), type, tag);
                        }
                    } else if (type == "checkbox" && isRequired && !$(el).attr("checked")) {
                        // 复选框是，只有要required就验证，木有就不管
                        allpass = remind(el, type, tag);
                    } else if (tag == "select" && isRequired && !el.value) {
                        // 下拉框只要关心值
                        allpass = remind(el, type, tag);
                    } else if ((isRequired && self.isEmpty(el)) || !(allpass = self.isRegex(el))) {
                        // 各种类型文本框以及文本域
                        // allpass为true表示是为空，为false表示验证不通过
                        allpass? remind(el, type, "empty"): remind(el, type, tag);
                        allpass = false;
                    } else if (self.isOverflow(el)) {
                        // 最大值最小值, 个数是否超出的验证
                        allpass = false;
                    }
                });
                
                return allpass;
            }
        };
    })();
    
    $.fn.extend({
        isVisible: function() {
            return $(this).attr("type") !== "hidden" && $(this).css("display") !== "none" && $(this).css("visibility") !== "hidden";
        },
        hasProp: function(prop) {
            if (typeof prop !== "string") return undefined;
            var hasProp = false;
            if (document.querySelector) {
                var attrProp = $(this).attr(prop);
                if (attrProp !== undefined && attrProp !== false) {
                    hasProp = true;
                }
            } else {
                // IE6, IE7
                var outer = $(this).get(0).outerHTML, part = outer.slice(0, outer.search(/\/?['"]?>(?![^<]*<['"])/));
                hasProp = new RegExp("\\s" + prop + "\\b", "i").test(part);
            }
            return hasProp;
        },
        selectRange: function(start, end) {
            var that = $(this).get(0);
            if ($.browser.msie) {
                var range = that.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            } else {
                that.focus();
                that.setSelectionRange(start, end);
            }
            return this;
        },
        testRemind: function(content, options) {
            var defaults = {
                size: 6,    // 三角的尺寸
                align: "center",    //三角的位置，默认居中
                css: {
                    maxWidth: 280,
                    backgroundColor: "#FFFFE0",
                    borderColor: "#F7CE39",
                    color: "#333",
                    fontSize: "12px",
                    padding: "5px 10px",
                    zIndex: 202
                }
            };
            
            options = options || {};
            options.css = $.extend({}, defaults.css, options.css || $.testRemind.css);
            
            var params = $.extend({}, defaults, options || {});
            
            // 如果元素不可见，不处理
            if (!content || !$(this).isVisible()) return;
            
            var objAlign = {
                "center": "50%",
                "left": "15%",
                "right": "85%"  
            }, align = objAlign[params.align] || "50%";
            
            params.css.position = "absolute";
            params.css.top = "-99px";
            params.css.border = "1px solid " + params.css.borderColor;
            
            if ($("#" + $.testRemind.id).size()) $.testRemind.hide();
            
            this.remind = $('<div id="'+ $.testRemind.id +'">'+ content +'</div>').css(params.css);
            $(document.body).append(this.remind);
            
            // IE6 max-width的处理
            var maxWidth;
            if (!window.XMLHttpRequest && (maxWidth = parseInt(params.css.maxWidth)) && this.remind.width() > maxWidth) {
                 this.remind.width(maxWidth);   
            }
            
            // 当前元素的位置，提示框的方向
            var offset = $(this).offset(), direction = "top";
            if (!offset) return $(this);
            var remindTop = offset.top - this.remind.outerHeight() - params.size;
            if (remindTop < $(document).scrollTop()) {
                direction = "bottom";
                remindTop = offset.top + $(this).outerHeight() + params.size;
            }   
            
            // 创建三角
            var fnCreateCorner = function(beforeOrAfter) {
                // CSS名称值与变量，主要用来mini后节约文件大小
                var transparent = "transparent", dashed = "dashed", solid = "solid";
                
                // CSS样式对象们
                var cssWithDirection = {}, cssWithoutDirection = {
                    // 与方向无关的CSS
                    //left: align,
                    width: 0,
                    height: 0,
                    overflow: "hidden",
                    //marginLeft: (-1 * params.size) + "px",
                    borderWidth: params.size + "px",
                    position: "absolute"
                }, cssFinalUsed = {};
                
                // before颜色为边框色
                // after为背景色
                // 方向由direction决定
                if (beforeOrAfter === "before") {
                    cssWithDirection = {
                        "top": {
                            borderColor: [params.css.borderColor, transparent, transparent, transparent].join(" "),
                            borderStyle: [solid, dashed, dashed, dashed].join(" "),
                            top: 0
                        },
                        "bottom": {
                            borderColor: [transparent, transparent, params.css.borderColor, ""].join(" "),
                            borderStyle: [dashed, dashed, solid, dashed].join(" "),
                            bottom: 0
                        }   
                    };  
                } else if (beforeOrAfter === "after") {
                    cssWithDirection = {
                        "top": {
                            borderColor: params.css.backgroundColor + ["", transparent, transparent, transparent].join(" "),
                            borderStyle: [solid, dashed, dashed, dashed].join(" "),
                            top: -1
                        },
                        "bottom": {
                            borderColor: [transparent, transparent, params.css.backgroundColor, ""].join(" "),
                            borderStyle: [dashed, dashed, solid, dashed].join(" "),
                            bottom: -1
                        }   
                    };  
                } else {
                    cssWithDirection = null;
                    cssWithoutDirection = null;
                    cssFinalUsed = null;
                    return null;    
                }
                
                cssFinalUsed = $.extend({}, cssWithDirection[direction], cssWithoutDirection);
                
                return $('<'+ beforeOrAfter +'></'+ beforeOrAfter +'>').css(cssFinalUsed);
            };
            
            // 限高
            var cssOuterLimit = {
                width: 2 * params.size,
                left: align,
                marginLeft: (-1 * params.size) + "px",
                height: params.size,
                textIndent: 0,
                overflow: "hidden",
                position: "absolute"
            };
            if (direction == "top") {
                cssOuterLimit["bottom"] = -1 * params.size;
            } else {
                cssOuterLimit["top"] = -1 * params.size;
            }
            
            this.remind.css({
                left: offset.left,
                top: remindTop, 
                // marginLeft: ($(this).outerWidth() - this.remind.outerWidth()) * 0.5 + /*因为三角位置造成的偏移*/ this.remind.outerWidth() * (50 - parseInt(align)) / 100        
                // 等于下面这个：
                marginLeft: $(this).outerWidth() * 0.5 - this.remind.outerWidth() * parseInt(align) / 100
            }).prepend($('<div></div>').css(cssOuterLimit).append(fnCreateCorner("before")).append(fnCreateCorner("after")));
            
            $.testRemind.display = true;
            
            // 绑定消除事件
            $.testRemind.bind();
            
            return $(this);
        },
        html5Validate: function(callback, options) {
            var defaults = {
                // 取消浏览器默认的HTML验证
                novalidate: true,
                // 禁用submit按钮可用
                submitEnabled: true,
                // 额外的其他验证
                validate: function() { return true; }
            };
            var params = $.extend({}, defaults, options || {});
            var elements = $(this).find(":input");
            if ($.html5Validate.isSupport) {
                if (params.novalidate) {
                    $(this).attr("novalidate", "novalidate");
                } else {
                    elements.each(function() {
                        var type = this.getAttribute("type") + "", typeReplaced = type.replace(/\W+$/, "");
                        if (type != typeReplaced) {
                            try { this.type = typeReplaced; } catch(e) {}
                        }
                    }); 
                    params.hasTypeNormally = true;
                }
            }
            
            // disabeld的submit按钮还原
            if (params.submitEnabled) {
                $(this).find(":disabled").each(function() {
                    if (/^image|submit$/.test(this.type)) {
                        $(this).removeAttr("disabled"); 
                    }
                });
            }
            
            $(this).bind("submit", function() {
                if ($.html5Validate.isAllpass(elements, params) && params.validate() && $.isFunction(callback)) {
                    callback.call(this);    
                }
                return false;   
            });
            
            return $(this);
        }
    });
})(jQuery);




$.testRemind.css = {
    "padding": "8px 10px",
    "borderColor": "#aaa",
    "borderRadius": "3px",
    "boxShadow": "2px 2px 4px rgba(0,0,0,.2)",
    "background": "#fff url(../static/img/chrome-remind.png) no-repeat 10px 12px",
    "backgroundColor": "#fff",
    "fontSize": "14px",
    "textIndent": "28px",
    "background-position": "10px 8px",
};


function showRespMsg(resp){
    if (typeof resp != 'object'){
        resp = JSON.parse(resp);
    }
    // if (resp.success === true){
    //     alert(resp.message);
    // }
    // else if (resp.success === false){
    //     alert(resp.message);
    // }
    alert(resp.message);
}


function get_city_list(){
    $.getJSON( "/city_list", function( data ) {
        var items = [];
        $.each( data.city_list, function( key, val ) {
            // console.log(key,val);
            items.push('<option value="'+ val.code + '">' + val.name +'</option>')
        });

        $(".city_code").append(items.join(''));
    });
}


function get_zone_list(obj){
    // console.log(obj);
    city_code = obj.value;
    thisForm  = $(obj).parents('form');
    // console.log('thisForm',thisForm);
    zone_list = thisForm.find('[name="zone_code"]');

    if (city_code == null){
        city_code = thisForm.find('select[name="city_code"]').val();
    }

    $.getJSON(
        '/backend/zone_list',
        {'city_code':city_code},
        function(data) {
            var items = [];

            $.each(data.zone_list, function(key,val) {
                // console.log(key,val);
                items.push('<option value="'+ val.code + '">' + val.name +'</option>')
            });

            zone_list.empty();
            zone_list.append(items.join(''));
        }
    )
}


function custom_list(arg){
    this_form = $(arg).parents('form')
    city_code = this_form.find('.city_list').val() ;
    zone_code = this_form.find('.zone_list').val();
    tag_code  = this_form.find('.tag_list').val();
    $.getJSON(
        "/backend/custom_list",
        {
            'city_code':city_code,
            'zone_code':zone_code,
            'tag_code':tag_code
        },
        function(data){
        var items = [];
        $.each( data.custom_list, function( key, val ) {
            // console.log(key,val);
            items.push('<option value="'+ val.code + '">' + val.name +'</option>')
        });
        // console.log(items.join('\n'));
        custom_list = $(".custom_list")
        custom_list.empty()
        custom_list.append(items.join(''));
    })
}


function get_custom(custom_code){
    // var custom_code        = $("#search-custom input[name='custom_code']").val()
    var getCutomButton     = $("button.get-custom-button");
    var doModifyButton     = $("button.do-modify");        
    var unlockEditButton   = $("button.unlock-edit");
    var cancelModifyButton = $("button.cancel-modify");
    var lockField          = $("fieldset");

    if (custom_code != '' && custom_code != null ){
        console.log("Request with 'Custom ID':%s",custom_code);
        $.getJSON(
            "../backend/get_custom",
            {'custom_code':custom_code},
            function(data){
                // console.log('data',data)
                if (data != null ){
                    // custom_manage_form = $("[name='manage_customs']");
                    // custom_code = custom_manage_form.find("[name='custom_code']");
                    // custom_name = custom_manage_form.find("[name='custom_name']");
                    custom_code = $('#Manager_Custom_ID');
                    custom_name = $('#Manager_Custom_Name');

                    custom_code.val(data.custom_code);
                    custom_name.val(data.custom_name);

                    lockField.attr('disabled',true);
                    doModifyButton.hide();
                    cancelModifyButton.hide();
                    unlockEditButton.show();
                }
                else{
                    lockField.attr('disabled',false);
                    custom_code = $('#Manager_Custom_ID');
                    custom_name = $('#Manager_Custom_Name');

                    custom_code.val('');
                    custom_name.val('');
                    console.warn('好像没对唷~');
                }

            }
        )
    }
    else{
        console.warn('你还什么都没有输入唷~')
    }
}


function do_modify(arg){
    custom_info_form = $("[name='manage_customs']");
    els = custom_info_form.find(":disabled")
    $.each(els,function(){
        $(this).attr('disabled',false)
        console.log(this)
        // v.setAttribute('disabled','false')
        // if(v.attr("disabled") == true){
        //     v.attr("disabled") = false
        // }
    })
}




$(document).ready(function(){

//事件 点击保存修改按钮
// $('button#btn-save-change').click(function(e){
//     e.preventDefault();
//     thisForm = $(this).parents('form');
//     $.post(
//         '../backend/custom-manager',
//         thisForm.serialize(),
//         function(data){
//             console.data
//         }
//         )
// })

//修改商户 表单验证 提交
$("form#edit-custom").html5Validate(function(){
    console.log(this);
    $.post(
        '../backend/custom-manager',
        $(this).serialize(),
        function(data,status){
            if (status === 'success'){
                resp = JSON.parse(data)
                if (resp.success === true){
                    alert(resp.message)
                    console.log(resp.message)
                }
                else{
                    alert('修改失败')
                    console.log('修改失败')
                }
            }
            else{
                alert('服务器可能出错了!')
            }
            // console.log('status',status)
            // console.log(data)
        }
    );
})


//事件 点击编辑按钮
$('button#btn-edit-custom').click(function(e){
    e.preventDefault();
    $(this).hide();
    thisForm = $(this).parents('form');
    thisForm.find('button#btn-delete-custom').hide();
    thisForm.find('fieldset').attr('disabled',false);
    thisForm.find('button#btn-change-img').attr('disabled',false);
    thisForm.find('button#btn-save-change').show().attr('disabled',false);
    thisForm.find('button#btn-cancel-change').show().attr('disabled',false);

})

//事件 改变城市列表
$('select[name="city_code"]').change(function(e){
    get_zone_list(this)
})

//事件 选中文件
$("[name='custom_img']").change(function(e){
    $(this).next().children().attr('disabled',false);
})

//事件 点击保存文件
$('button[name="btn-upload-img"]').click(function(e){
    e.preventDefault();
    thisForm = $(this).parents("form");
    console.log(thisForm.find(':file')[0].files)
    // progressBar = thisForm.find('[role="progressbar"]')
    file = thisForm.find('input[name="custom_img"]')[0].files[0]

    if (file != null){
        thisForm.find('[name="toggle-progress"]').show();
        $("#toggle-progress").toggle();
        FileUpload(thisForm);
    }
    else{
        console.warn('狗日的,要先选择文件才可以!')
    }
})

//事件 点击改变商户图片 btn-change-img
$('button[name="btn-change-img"]').click(function(e){
    $(this).hide();                                     //隐藏自身
    thisForm = $(this).parents('form');
    thisForm.find('[name="custom_img_url"]').hide();    //隐藏图片地址输入框
    thisForm.find('[name="custom_img"]').show();        //显示图片选择框
    thisForm.find('[name="btn-upload-img"]').attr('disabled',false).show();    //显示并激活图片上传按钮

})


//获取商户信息 校验表单
$('form#get-custom-form').html5Validate(function(){
    console.log(this)
    custom_code = $("[name='get_custom_code']").val();

    $.getJSON(
        '../backend/get_custom',
        // {
        //     'custom_code':custom_code
        // },
        $(this).serialize(),
        function(data){
            //返回数据不为空，数据话初始化入表单
            if (data != null){
                editForm(data);
                console.log('获取数据成功\n >> data : %O',data);
                $('button#btn-save-change').hide();                             //隐藏保存按钮，即使未显示
                $('button#btn-cancel-change').hide();                           //隐藏取消按钮，即使未显示
                $('input#change-custom-img-url').attr('readonly',true)          //设置图片地址输入框为 **只读**
                $('#btn-change-img').show().attr('disabled',true);             //显示并禁用图片编辑按钮
                $("#btn-edit-custom").show().attr('disabled',false);            //激活编辑按钮
                $('button#btn-delete-custom').show().attr('disabled',false);    //激活删除按钮
            }
            //返回数据为空
            else{
                console.warn('数据返回错误\n>> data : %O',data)
            }
        }
    )
})
//事件 获取商户信息
// $("#get-custom-info").click(function(e){
//     custom_code = $("[name='get_custom_code']").val();

//     $.getJSON(
//         '../backend/get_custom',
//         {
//             'custom_code':custom_code
//         },
//         function(data){
//             //返回数据不为空，数据话初始化入表单
//             if (data != null){
//                 editForm(data);
//                 console.log('获取数据成功\n >> data : %O',data);
//                 $('button#btn-save-change').hide();                             //隐藏保存按钮，即使未显示
//                 $('button#btn-cancel-change').hide();                           //隐藏取消按钮，即使未显示
//                 $('input#change-custom-img-url').attr('readonly',true)          //设置图片地址输入框为 **只读**
//                 $('#btn-change-img').show().attr('disabled',false);             //显示并激活图片编辑按钮
//                 $("#btn-edit-custom").show().attr('disabled',false);            //激活编辑按钮
//                 $('button#btn-delete-custom').show().attr('disabled',false);    //激活删除按钮
//             }
//             //返回数据为空
//             else{
//                 console.warn('数据返回错误\n>> data : %O',data)
//             }
//         }
//         )
// })

//函数 填充商户管理表单
//@param : json object
function editForm(obj){
    form = $('form[id="edit-custom"]');
    var lockFieldset     = form.find('fieldset');                        //fieldset区域
    var custom_code      = form.find('input[name="custom_code"]');       //商户ID
    // var custom_code_show = form.find('input[name="custom_code_show"]');  //商户ID,显示的，不被提交
    var custom_name      = form.find('input[name="custom_name"]');       //商户名
    var city_code        = form.find('select[name="city_code"]');        //城市
    var zone_code        = form.find('select[name="zone_code"]');        //区域
    var tag_code         = form.find('select[name="tag_code"]');         //类型
    var custom_img_url   = form.find('input#change-custom-img-url');     //图片地址输入框
    var custom_img_up    = form.find('button#btn-upload-img');           //图片上传按钮
    var custom_img_input = form.find('input#change-custom-img');         //图片选择框
    var items            = [];

    $.each(obj.zones,function(sn,_obj){
        items.push('<option value="'+ _obj.code + '">' + _obj.name +'</option>');
    })
    zone_code.empty();
    zone_code.append(items.join(''));

    lockFieldset.attr('disabled',true);                               //锁定表单fieldset内的元素
    custom_code.val(obj.custom_code);
    custom_code.attr('readonly',true).val(obj.custom_code);     //设置商户ID，并且只读
    custom_name.val(obj.custom_name);                           //设置商户名
    city_code.val(obj.city_code);                               //设置城市
    zone_code.val(obj.zone_code);                               //设置区域
    tag_code.val(obj.tag_code);                                 //设置类型   
    custom_img_url.val(obj.last_url).attr('readonly',true).show()      //设置图片地址，且只读
    custom_img_input.hide();
    custom_img_up.hide();               //隐藏图片上传按钮
}

//事件 点击放弃修改
$("#btn-cancel-change").click(function(e){
    thisForm = $(this).parents('form');
    thisForm[0].reset()
    $(this).hide();         //隐藏自身
    thisForm.find('button').attr('disabled',true)       //禁用表单内所有按钮
    thisForm.find('button').hide();
    thisForm.find('button#btn-edit-custom').show();     //显示编辑按钮
    thisForm.find('button#btn-delete-custom').show();   //显示删除按钮
    thisForm.find('button#btn-change-img').show()       //显示图片编辑按钮
    thisForm.find('fieldset').attr('disabled',false);                   //解锁fieldset
    thisForm.find('input[name="custom_code"]').attr('readonly',false);  //激活商户ID输入框
    thisForm.find('input').show();                      //显示所有输入框
    thisForm.find('input[name="custom_img"]').hide()    //隐藏图片选择框
    thisForm.find('[name="toggle-progress"]').hide()    //隐藏进度条
    thisForm.find('[name="progressbar"]').css('width','0%');    //进度条重置到0%
})

//添加商户 校验表单
$('form#add-custom').html5Validate(function(){
    console.log($(this).serialize())

    $.ajax({
        type    : "put",
        url     : "../backend/custom-manager",
        data    : $(this).serialize(),
        success : function(resp,status){
            if (status === 'success'){
                resp = JSON.parse(resp)
                if (resp.success === true){
                    alert(resp.message)
                }
            }
        }
    })
})
//事件 点击添加商户按钮
// $("#btn-add-custom").click(
//     function(e){
//     e.preventDefault();
//     thisForm = $(this).parents("form");
//     $.ajax({
//         type    : "put",
//         url     : "../backend/custom-manager",
//         data    : thisForm.serialize(),
//         success : function(data,status){
//             console.log(data);
//             console.log(status)
//         }
//     })
// })

//事件 点击放弃提交
$("#btn-reset-form").click(function(e){
    thisForm = $(this).parents('form');
    thisForm.find("#save_img").attr('disabled',true);
    thisForm.find('[name="progressbar"]').css('width','0%');
    thisForm.find('[name="toggle-progress"]').hide();
    console.warn('你放弃了本次提交:');
    //放弃内容
    $.each(thisForm.serializeArray(),function(k,v){console.log(k,v)});

})

//函数 HTML5上传文件
function FileUpload(thisForm) {
    console.log(thisForm);
    var File           = thisForm.find(':file')[0].files[0];
    var progressBar    = thisForm.find('[name="progressbar"]');
    var custom_img_url = thisForm.find('input[name="custom_img_url"]');

    var UpFiles = new FormData();
    var xhr     = new XMLHttpRequest();


    UpFiles.append("fileUpload",file);
    if (xhr.upload){
        //上传进度 (上传中)
        xhr.upload.onprogress = function(evt){
            if(evt.lengthComputable){
                // progressBar.value = (evt.loaded/evt.total)*100;
                // progressBar.textContent = progressBar.value;
                progress = (evt.loaded/evt.total) * 100;
                progressBar.css('width',progress + '%');
            }
        }

        //状态 上传结果(成功/失败)
        xhr.onreadystatechange = function(e){
            if(xhr.readyState ==4){
                if (xhr.status ==200){
                    console.log(xhr.responseText);
                    try{
                        resp = JSON.parse(xhr.responseText);
                        if (resp.success === true){
                            //如果返回true 设置input[name="custom_img_url"]的值
                            // console.log(resp.img_url);
                            custom_img_url.val(resp.img_url);
                            alert('文件上传成功！')
                        }
                        else{
                            throw {'message':'保存文件失败!'}
                        }
                    }
                    catch(err){
                        alert(err.message)
                        console.error(err.message)

                    }
                }
                else{
                    alert('上传过程中出错了！')
                    console.log('上传过程中出错了！')
                }
            }
        }

    }

    // xhr.onload = function () {
    //     console.log(xhr.upload)
    //     // fUpFles = new FormData();
    //     console.log(xhr.readyState)
    //     console.log(xhr.responseText);
    // };

    // progressBar = thisForm.find('[role="progressbar"]')
    // xhr.upload.onprogress = function(evt){
    //     if(evt.lengthComputable){
    //         // progressBar.value = (evt.loaded/evt.total)*100;
    //         // progressBar.textContent = progressBar.value;
    //         progress = (evt.loaded/evt.total) * 100;
    //         progressBar.css('width',progress + '%');
    //     }
    // }

    //开始上传文件
    xhr.open("post", "/backend/uploads", true);
    xhr.send(UpFiles);
}

})