#!/usr/bin/env python
#coding:utf-8


import os




class Site_Info():
    SITE_NAME     = u'Sigma'
    SITE_KEYWORDS = """"""
    SITE_DESC     = """"""
    DOMAIN        = 'http://0.0.0.0:9000'




class App_Config():
    DEBUG = True
    COOKIE_SECRET =u'Your never guess!'




class File_Upload_Config():
    # UPLOAD_PATH      = 
    ALLOWED_EXTENSIONS = set(['png','jpg','jpeg'])
    MAX_CONTENT_LENGTH = 1*1024*1024




class DB_Config():
    DB_ENGINE = 'mysql'
    DB_SCHEME = 'mysqldb'
    DB_HOST   = '127.0.0.1'
    DB_PORT   = '33060'
    DB_USER   = 'root'
    DB_PASSWD = 'lneoe'
    DB_NAME   = 'sigma'              #os.path.join(os.path.dirname(__file__),'sigma.db')
    DB_CONNECT_STRING = DB_ENGINE + '+' + DB_SCHEME + '://' +\
                        DB_USER + ':' + DB_PASSWD + '@' +\
                        DB_HOST + ':' + DB_PORT +'/' + DB_NAME + '?charset=utf8'          #'mysql+mysqldb://root:lneoe@127.0.0.1:33060/sigma?charset=utf8'
    # DB_CONNECT_STRING = 'mysql+mysqldb://' + DB_NAME + ':' + DB_PASSWD + '@' +\
    #                     DB_HOST +':' + DB_PORT + '/' + DB_NAME + '?charset=utf8'




class Redis_Config(object):
    HOST = '127.0.0.1'
    PORT = 63790
    DB   = 0


class SMTP_Config():
    ADMIN_EMAIL    = 'joe@ihex.me'
    SMTP_SERVER    = 'smtp.gmail.com'
    SMTP_PORT      = 587
    SMTP_USER      = 'joe@ihex.me'
    SMTP_PASSWORD  = ''
    SMTP_USETLS    = True